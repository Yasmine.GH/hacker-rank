package main

import (
    "bufio"
    "fmt"
    "io"
    "os"
    "strings"
    "math"
    "log"
    "unicode"
)

/*
 * Complete the 'camelcase' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts STRING s as parameter.
 */

func camelcase(s string) int32 {
    
    SLen := len(s)
    
    if(SLen<1 || SLen> int(math.Pow(float64(10), float64(5)))){
        log.Fatal("The string length must bebetween 1 and 10 power 5")
    }
    var words  []string
    words = append(words , "")
    for _, c := range s{
        if(unicode.IsUpper(c)){
              words = append(words, string(c))
        }
    }
    return int32(len(words))
}

func main() {
    reader := bufio.NewReaderSize(os.Stdin, 16 * 1024 * 1024)

    stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
    checkError(err)

    defer stdout.Close()

    writer := bufio.NewWriterSize(stdout, 16 * 1024 * 1024)

    s := readLine(reader)

    result := camelcase(s)

    fmt.Fprintf(writer, "%d\n", result)

    writer.Flush()
}

func readLine(reader *bufio.Reader) string {
    str, _, err := reader.ReadLine()
    if err == io.EOF {
        return ""
    }

    return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
    if err != nil {
        panic(err)
    }
}


