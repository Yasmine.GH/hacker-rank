package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	"log"
)

/*
 * Complete the 'caesarCipher' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts following parameters:
 *  1. STRING s
 *  2. INTEGER k
 */

func caesarCipher(s string, k int32) (res string) {
	// Write your code here
	res = ""
	SLen := len(s)
	if SLen < 1 || SLen > 100 {
		log.Fatal(" the string length must be between 1 and 100")
	}

	if k < 0 || SLen > 100 {
		log.Fatal(" K must be between 0 and 100")
	}
	runeS := []rune(s)

	for _, c := range runeS {
		asciiValue := int(c)
		res = res + string(newAsciiCode(asciiValue, int(k)))
	}
	return
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 16*1024*1024)

	stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
	checkError(err)

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 16*1024*1024)

	nTemp, err := strconv.ParseInt(strings.TrimSpace(readLine(reader)), 10, 64)
	checkError(err)
	n := int32(nTemp)
	if n < 1 || n > 100 {
		log.Fatal(" the string length must be between 1 and 100")
	}
	s := readLine(reader)

	kTemp, err := strconv.ParseInt(strings.TrimSpace(readLine(reader)), 10, 64)
	checkError(err)
	k := int32(kTemp)

	result := caesarCipher(s, k)
	fmt.Fprintf(writer, "%s\n", result)

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func newAsciiCode(code int, k int) int {
	newCode := code + k

	var res int
	if code >= int('A') && code <= int('Z') {
		if newCode > int('Z') {
			res = code
			for i := 1; i <= k; i++ {
				res++
				if res > int('Z') {
					res = int('A')
				}
			}
		} else {
			res = newCode
		}
	} else if code >= int('a') && code <= int('z') {
		if newCode > int('z') {
			res = code
			for i := 1; i <= k; i++ {
				res++
				if res > int('z') {
					res = int('a')
				}
			}
		} else {
			res = newCode
		}
	} else {
		res = code
	}
	return res

}
